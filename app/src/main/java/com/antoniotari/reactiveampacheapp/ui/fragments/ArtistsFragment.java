/*
 * Power Ampache, Ampache player for Android
 * Copyright (C) 2016  Antonio Tari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.antoniotari.reactiveampacheapp.ui.fragments;

import android.app.Activity;

import java.util.ArrayList;
import java.util.List;

import com.antoniotari.reactiveampache.api.AmpacheApi;
import com.antoniotari.reactiveampache.models.Artist;
import com.antoniotari.reactiveampache.models.Tag;
import com.antoniotari.reactiveampache.models.Tags;
import com.antoniotari.reactiveampacheapp.ui.adapters.ArtistsAdapter;
import com.antoniotari.reactiveampacheapp.ui.adapters.ArtistsAdapter.OnArtistClickListener;

import rx.android.observables.AndroidObservable;

/**
 * Created by antonio tari on 2016-05-21.
 */
public class ArtistsFragment extends BaseFragment {
    private ArtistsAdapter mArtistsAdapter;
    private OnArtistClickListener mOnArtistClickListener;
    private List<Artist> mArtists;
    private List<Artist> mOriginalArtists;

    public ArtistsFragment() {
    }

    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);
        if(activity instanceof OnArtistClickListener){
            mOnArtistClickListener = (OnArtistClickListener) activity;
        } else {
            throw new RuntimeException("activity MUST implement OnArtistClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mOnArtistClickListener = null;
    }

    @Override
    protected void initialize() {
        if (mArtistsAdapter == null || mArtistsAdapter.getItemCount() == 0) {
            startWaiting();
        }
        AndroidObservable.bindFragment(this,
                AmpacheApi.INSTANCE.getArtists())
                .subscribe(this::initAdapter, this::onError);
    }

    @Override
    protected void onRefresh() {
        AndroidObservable.bindFragment(this,
                AmpacheApi.INSTANCE.handshake()
                        .flatMap(handshakeResponse -> AmpacheApi.INSTANCE.getArtists()))
                .subscribe(artists -> {
                    super.resetFilterList();
                    initAdapter(artists);
                    stopWaiting();
                }, this::onError);
    }

    private void initAdapter(List<Artist> artistList) {
        mArtists = artistList;
        // once we have the album list determine if fast scroll is needed
        mFastScrollWrapper.determineSetFastScroll(artistList.size());

        stopWaiting();
        if(mArtistsAdapter == null){
            mArtistsAdapter = new ArtistsAdapter(artistList);
            mArtistsAdapter.setOnArtistClickListener(mOnArtistClickListener);
            recyclerView.setAdapter(mArtistsAdapter);
        } else {
            mArtistsAdapter.setArtists(artistList);
            mArtistsAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void filterList(final Tag tag) {
        super.filterList(tag);

        if (mOriginalArtists == null)
            mOriginalArtists = new ArrayList<>(mArtists);

        List<Artist> artists = Tags.filterListByTag(mOriginalArtists, tag.getId());
        initAdapter(artists);
    }

    @Override
    public void resetFilterList() {
        super.resetFilterList();
        if (mOriginalArtists == null) return;
        initAdapter(mOriginalArtists);
    }
}