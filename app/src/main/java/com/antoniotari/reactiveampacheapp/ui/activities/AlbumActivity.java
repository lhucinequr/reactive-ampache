/*
 * Power Ampache, Ampache player for Android
 * Copyright (C) 2016  Antonio Tari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.antoniotari.reactiveampacheapp.ui.activities;

import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import com.antoniotari.android.lastfm.LastFm;
import com.antoniotari.android.lastfm.LastFmAlbum;
import com.antoniotari.android.lastfm.LastFmBase;
import com.antoniotari.reactiveampache.api.AmpacheApi;
import com.antoniotari.reactiveampache.models.Album;
import com.antoniotari.reactiveampache.models.Song;
import com.antoniotari.reactiveampache.models.Sortable;
import com.antoniotari.reactiveampache.models.Sortable.SortOption;
import com.antoniotari.reactiveampache.utils.Log;
import com.antoniotari.reactiveampacheapp.R;
import com.antoniotari.reactiveampacheapp.managers.PlayManager;
import com.antoniotari.reactiveampacheapp.managers.PlaylistManager;
import com.antoniotari.reactiveampacheapp.ui.adapters.SongsAdapter;
import com.antoniotari.reactiveampacheapp.ui.adapters.SongsAdapter.OnAddToPlaylistClickListener;
import com.antoniotari.reactiveampacheapp.utils.LastFmCache;
import com.antoniotari.reactiveampacheapp.utils.Utils;

import rx.Observable;
import rx.Observable.OnSubscribe;
import rx.Subscriber;
import rx.android.observables.AndroidObservable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by antonio tari on 2016-05-22.
 */
public class AlbumActivity extends SortableActivity {

    public static final String KEY_INTENT_ALBUM = "com.antoniotari.reactiveampacheapp.albumactivity.album";

    private Album mAlbum;
    protected List<Song> mSongList;
    protected SongsAdapter mSongsAdapter;
    private LastFmAlbum mLastFmAlbum;
    private Song selectedSong = null;

    @Override
    protected void initOnCreate() {
        mAlbum = getIntent().getParcelableExtra(KEY_INTENT_ALBUM);
        mLastFmArtist = getIntent().getParcelableExtra(KEY_INTENT_LASTFM_ARTIST);

        setDetailName(mAlbum.getName());

        //TODO don't use the cache, pass the value as a parcelable
        // if the last fm info is in the cache load it otherwise fetch it
        LastFmBase lastFmBase = LastFmCache.INSTANCE.getFromCache(mAlbum.getArtist().getName()+mAlbum.getName());
        if (lastFmBase != null && (lastFmBase instanceof LastFmAlbum)) {
            mLastFmAlbum = (LastFmAlbum) lastFmBase;
            Utils.loadImage(mLastFmAlbum.getImages().getBiggestAvailable(), mToolbarImageView);
        } else {
            Observable.create(new OnSubscribe<LastFmAlbum>(){

                @Override
                public void call(final Subscriber<? super LastFmAlbum> subscriber) {
                    try {
                        LastFmAlbum lastFmArtist = LastFm.INSTANCE.getAlbumInfo(mAlbum.getArtist().getName(), mAlbum.getName());
                        if (lastFmArtist == null) throw new NullPointerException("last fm response is null");

                        if (!subscriber.isUnsubscribed()) {
                            subscriber.onNext(lastFmArtist);
                            subscriber.onCompleted();
                        }
                    } catch (Exception e) {
                        if (!subscriber.isUnsubscribed()) {
                            subscriber.onError(e);
                        }
                    }
                }
            }).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(lastFmAlbum -> {
                        mLastFmAlbum = lastFmAlbum;
                        String imageUrl = mLastFmAlbum.getImages().getBiggestAvailable();
                        if (TextUtils.isEmpty(imageUrl)) {
                            imageUrl = mAlbum.getArt();
                        }
                        Utils.loadImage(imageUrl, mToolbarImageView);
                    }, throwable -> Utils.loadImage(mAlbum.getArt(), mToolbarImageView));
        }

        AndroidObservable.bindActivity(this,
                AmpacheApi.INSTANCE.getSongsFromAlbum(mAlbum.getId()))
                .subscribe(this::initAdapter, this::onError);

        if (mLastFmArtist != null) {
            loadArtistImage(mLastFmArtist, mBackgroundImage, true);
        } else {
            // get artist info from last.fm
            artistInfoService(mAlbum.getArtist().getName())
                    .subscribe(lastFmArtist -> {
                        mLastFmArtist = lastFmArtist;
                        loadArtistImage(lastFmArtist, mBackgroundImage, true);
                    }, this::onError);
        }
    }

    @Override
    protected LayoutManager getRecyclerViewLayoutManager() {
        return new LinearLayoutManager(getApplicationContext());
    }

    @Override
    protected void onRefresh() {
        AndroidObservable.bindActivity(this,
                AmpacheApi.INSTANCE.handshake()
                        .flatMap(handshakeResponse -> AmpacheApi.INSTANCE.getSongsFromAlbum(mAlbum.getId())))
                .subscribe(songList -> {
                    initAdapter(songList);
                    swipeLayout.setRefreshing(false);
                }, this::onError);
    }

    protected void initAdapter(List<Song> songList) {
        mSongList = songList;
        stopWaiting();
        mSongsAdapter = new SongsAdapter(songList, (OnAddToPlaylistClickListener) this::addToPlaylist, this::onSongMenuClicked);
        showArtistNameInAdapter(mSongsAdapter);
        recyclerView.setAdapter(mSongsAdapter);

        // once we have the album list determine if fast scroll is needed
        mFastScrollWrapper.determineSetFastScroll(songList.size());
    }

    protected void showArtistNameInAdapter(SongsAdapter songsAdapter) {
        songsAdapter.setShowArtist(false);
    }

    private void addToPlaylist(Song song, int position) {
        recyclerView.showContextMenu();
        selectedSong = song;
    }

    @Override
    public boolean onContextItemSelected(final MenuItem item) {
        Log.blu(item.getItemId());
        switch (item.getItemId()) {
            case -1110:
                // play next
                PlayManager.INSTANCE.playNext(selectedSong);
                break;
            case -1111:
                // create new playlist
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                final EditText edittext = new EditText(this);
                alert.setTitle(R.string.playlist_dialog_title);
                alert.setView(edittext);
                alert.setPositiveButton(android.R.string.ok,
                        (dialog, whichButton) -> {
                            String playlistName = edittext.getText().toString();
                            PlaylistManager.INSTANCE.newPlaylist(playlistName);
                            PlaylistManager.INSTANCE.addToPlaylist(playlistName,selectedSong);
                            Toast.makeText(getApplicationContext(),R.string.playlist_created_toast,Toast.LENGTH_LONG).show();
                        });
                alert.setNegativeButton(android.R.string.cancel, (dialog, whichButton) ->{});
                alert.show();
                break;
            default:
                String playlistName = PlaylistManager.INSTANCE.getPlaylistNames().get(item.getItemId());
                PlaylistManager.INSTANCE.addToPlaylist(playlistName,selectedSong);
                Toast.makeText(getApplicationContext(), R.string.added_to_playlists,Toast.LENGTH_LONG).show();
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(final ContextMenu menu, final View v, final ContextMenuInfo menuInfo) {
        menu.setHeaderTitle(R.string.playlist_add_menu);
        List<String> playlists = PlaylistManager.INSTANCE.getPlaylistNames();
        int i = 0;
        for(String name:playlists) {
            menu.add(0, i++, 0, name);
        }
        menu.add(0, -1110, 0, R.string.play_next_menu); //groupId, itemId, order, title
        menu.add(0, -1111, 0, R.string.playlist_create_menu);

        // inflate menu
        //MenuInflater inflater = v.getContext().getMenuInflater();
        //inflater.inflate(R.menu.my_context_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            try {
                if (mLastFmAlbum != null && mLastFmAlbum.getBio() != null) {
                    showDialog(mLastFmAlbum.getName(), Utils.fromHtml(mLastFmAlbum.getBio().content));
                    return true;
                }
            }catch (Exception e){}
        }

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected SortOption getDefaultSortOption() {
        return SortOption.NAME;
    }

    @Override
    protected List<? extends Sortable> getSortableList() {
        return mSongList;
    }

    @Override
    protected SortOption[] getSortOptions() {
        return SortOption.values();
    }

    @Override
    protected Adapter getAdapter() {
        return mSongsAdapter;
    }

    @Override
    protected String getSortTagName() {
        return getString(R.string.sort_tagname_track);
    }
}
