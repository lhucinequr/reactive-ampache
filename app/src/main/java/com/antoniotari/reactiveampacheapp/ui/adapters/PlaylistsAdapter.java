/*
 * Power Ampache, Ampache player for Android
 * Copyright (C) 2016  Antonio Tari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.antoniotari.reactiveampacheapp.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import com.antoniotari.reactiveampache.models.Playlist;
import com.antoniotari.reactiveampacheapp.R;
import com.antoniotari.reactiveampacheapp.managers.PlaylistManager;
import com.antoniotari.reactiveampacheapp.models.LocalPlaylist;

import static com.antoniotari.reactiveampacheapp.utils.Utils.setHtmlString;

/**
 * Created by antoniotari on 2016-06-21.
 */
public class PlaylistsAdapter extends SectionIndexerAdapter<Playlist, SongViewHolder> {

    public interface OnPlaylistClickListener {
        void onPlaylistClick(final Playlist playlist);
    }

    private List<Playlist> mPlaylists;
    private OnPlaylistClickListener mOnPlaylistClickListener;

    public PlaylistsAdapter(List<Playlist> playlists) {
        mPlaylists = playlists;
    }

    public void setPlaylists(List<Playlist> playlists) {
        mPlaylists = playlists;
    }

    public void setOnPlaylistClickListener(final OnPlaylistClickListener onPlaylistClickListener) {
        mOnPlaylistClickListener = onPlaylistClickListener;
    }

    @Override
    public SongViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_song, viewGroup, false);
        return new SongViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SongViewHolder viewHolder, int i) {
        final Playlist playlist = mPlaylists.get(i);
        String title = playlist.getName();
        setHtmlString(viewHolder.songName, title);
        Context context = viewHolder.songName.getContext();
        String songStr = playlist.getItems() == 1 ? context.getString(R.string.song) : context.getString(R.string.songs);
        viewHolder.songDuration.setText(String.format(songStr, playlist.getItems()));

        viewHolder.mainCardView.setOnClickListener(view -> {
            if (mOnPlaylistClickListener != null) {
                mOnPlaylistClickListener.onPlaylistClick(playlist);
            }
        });

        // do not show the menu
        viewHolder.songMenuButton.setVisibility(View.GONE);

        if(playlist instanceof LocalPlaylist) {
            viewHolder.removePlaylist.setVisibility(View.VISIBLE);
            viewHolder.removePlaylist.setOnClickListener(v ->
                    Toast.makeText(viewHolder.removePlaylist.getContext(),R.string.playlist_long_press_remove, Toast.LENGTH_LONG).show());

            viewHolder.removePlaylist.setOnLongClickListener(v -> PlaylistManager.INSTANCE.removePlaylist(title));
        } else {
            viewHolder.removePlaylist.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        if (mPlaylists == null) {
            return 0;
        }
        return mPlaylists.size();
    }

    @Override
    protected List<Playlist> getItems() {
        return mPlaylists;
    }
}
